Source: r-cran-tidytext
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-cli,
               r-cran-dplyr (>= 1.1.1),
               r-cran-generics,
               r-cran-janeaustenr,
               r-cran-lifecycle,
               r-cran-matrix,
               r-cran-purrr,
               r-cran-rlang,
               r-cran-stringr,
               r-cran-tibble,
               r-cran-tokenizers,
               r-cran-vctrs
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-tidytext
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-tidytext.git
Homepage: https://cran.r-project.org/package=tidytext
Rules-Requires-Root: no

Package: r-cran-tidytext
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R text mining using 'dplyr', 'ggplot2', and other tidy tools
 Using tidy data principles can make many text mining tasks easier,
 more effective, and consistent with tools already in wide use. Much of the
 infrastructure needed for text mining with tidy data frames already exists
 in packages like 'dplyr', 'broom', 'tidyr', and 'ggplot2'. This package
 provides functions and supporting data sets to allow conversion of text
 to and from tidy formats, and to switch seamlessly between tidy tools and
 existing text mining packages.
